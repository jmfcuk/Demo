package com.demo.test.john.demo;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class HomeActivityA extends AppCompatActivity
         implements GoogleApiClient.ConnectionCallbacks,
                    GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient _googleApiClient;
    private Location _lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homea);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        final ListView listview = (ListView)findViewById(R.id.userList);

        ArrayList<User> list = User.getDummies();

        User[] users = list.toArray(new User[list.size()]);

        final UserListAdapter adapter =
                new UserListAdapter(this, users);

        listview.setAdapter(adapter);

        if (_googleApiClient == null) {
            _googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        ListView lv = (ListView)findViewById(R.id.userList);

        lv.setOnItemClickListener( new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               User tmp = (User) parent.getItemAtPosition(position);
           }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    protected void onStart() {
        _googleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        _googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // An unresolvable error has occurred and a connection to Google APIs
        // could not be established. Display an error message, or handle
        // the failure silently

        // ...
    }

    @Override
    public void onConnected(Bundle bundle) {

        try {
            _lastLocation = LocationServices.FusedLocationApi.getLastLocation(_googleApiClient);

            if (_lastLocation != null) {
                //mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
                //mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
            }
        }
        catch(SecurityException sex) {
            String s = sex.getMessage();
        }
        catch(Exception ex) {
            String s = ex.getMessage();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
