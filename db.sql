CREATE DATABASE `demo` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `userprofile` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) DEFAULT NULL,
  `username` varchar(1024) DEFAULT NULL,
  `intro` longtext,
  `blurb` longtext,
  `pic` longtext,
  `keywords` longtext,
  `ignore_keywords` longtext,
  `last_loc` point DEFAULT NULL,
  `is_logged_in` bit(1) DEFAULT NULL,
  `last_log_in` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

