package com.demo.test.john.demo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by John on 20/11/2016.
 */

public final class Prefs {

    private Prefs() {}

    public static boolean getKeepSignedIn(Context ctx) {

        SharedPreferences prefs =
                ctx.getSharedPreferences(ctx.getString(R.string.app_shared_preferences_name), ctx.MODE_PRIVATE);

        return prefs.getBoolean(ctx.getString(R.string.pref_keep_signed_in), false);
    }

    public static void setKeepSignedIn(Context ctx, boolean b) {

        SharedPreferences prefs =
                ctx.getSharedPreferences(ctx.getString(R.string.app_shared_preferences_name), ctx.MODE_PRIVATE);

        SharedPreferences.Editor ed = prefs.edit();

        ed.putBoolean(ctx.getString(R.string.pref_keep_signed_in), b);

        ed.commit();
    }
}
