package com.demo.test.john.demo;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by John on 21/11/2016.
 */

public class User {

    public UUID UserId;

    public String Username;

    public String Intro;

    public String Blurb;

    public String Pic;

    public ArrayList<String> Keywords;

    public ArrayList<String> IgnoreKeywords;

    public User() { super(); }

    public User(UUID userId,
                String username,
                String intro,
                String blurb,
                String pic,
                ArrayList<String> keywords,
                ArrayList<String> ignoreKeywords) {

        UserId = userId;
        Username = username;
        Intro = intro;
        Blurb = blurb;
        Pic = pic;
        Keywords = keywords;
        IgnoreKeywords = ignoreKeywords;
    }

    public static ArrayList<User> getDummies() {

        ArrayList<User> list = new ArrayList<User>();

        for (Integer i = 0; i < 7; ++i) {

            String idx = i.toString();

            list.add(new User(UUID.randomUUID(),
                              "User " + idx,
                              "Intro " + idx,
                              "Blurb " + idx,
                               "Pic " + idx,
                               null,
                               null));
        }

        return list;
    }
}

