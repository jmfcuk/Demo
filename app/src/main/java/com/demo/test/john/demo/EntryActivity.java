package com.demo.test.john.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class EntryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);

        Intent intent = null;

        if(!Prefs.getKeepSignedIn(this))
            intent = new Intent(this, LoginActivity.class);
        else
            intent = new Intent(this, HomeActivityA.class);

        startActivity(intent);

        finish();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}

