package com.demo.test.john.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by John on 21/11/2016.
 */

public class UserListAdapter extends ArrayAdapter<User> {

    private final Context _context;
    private final User[] _users; //ArrayList<User> _users;


    public UserListAdapter(Context context, User[] users) {
        super(context, R.layout.user_list_item, users);
        _context = context;
        _users = users;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = null;

        try{
            LayoutInflater inflater = (LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(R.layout.user_list_item, parent, false);

            TextView tvUsername = (TextView) rowView.findViewById(R.id.username);
            TextView tvIntro = (TextView) rowView.findViewById(R.id.intro);

            tvUsername.setText(_users[position].Username);
            tvIntro.setText(_users[position].Intro);
        }
        catch(Exception e) {
            String s = e.getMessage();
        }

        return rowView;
    }
}

